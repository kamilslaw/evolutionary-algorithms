# coding=utf-8
"""
Usage:
    reporter.py report --sequences <sequences> --bound_time <bound_time>
    reporter.py report_range --sequence_range <sequence_range> --sequence_step <sequence_step> --bound_time <bound_time>

Arguments:
    path                    Path to directory containing report files
    paths                   Paths to directories containing report files

Options:
    -h --help               Display this help and exit
    --version               Show version

"""
import time
import os
from docopt import docopt
import subprocess
from progressbar import progressbar

import plotly.plotly as py
import plotly.graph_objs as go


def run_sdls(seq_len: int, bound_time: int) -> [str]:
    process = subprocess.Popen(
        ['/home/a/Documents/EA/evolutionary-algorithms/Project/App/target/release/sdls', str(seq_len), str(bound_time)],
        stdout=subprocess.PIPE)

    print('Calculating sequence for length: %s, bound time: %s [min]' % (seq_len, bound_time))
    for _ in progressbar(range(bound_time * 60)):
        if process.poll() is None:
            time.sleep(1)
        else:
            continue

    std_out, std_err = process.communicate()
    return [int(energy_value) for energy_value in filter(lambda x: x.isdigit(), str(std_out, 'utf-8').split('\n'))]


def get_asymptote_value(seq_len):
    if seq_len < 4 or seq_len > 60:
        raise RuntimeError('Unsupported sequence length...')
    else:
        return {
            4: 2,
            5: 2,
            6: 7,
            7: 3,
            8: 8,
            9: 12,
            10: 13,
            11: 5,
            12: 10,
            13: 6,
            14: 19,
            15: 15,
            16: 24,
            17: 32,
            18: 25,
            19: 29,
            20: 26,
            21: 26,
            22: 39,
            23: 47,
            24: 36,
            25: 36,
            26: 45,
            27: 37,
            28: 50,
            29: 62,
            30: 59,
            31: 67,
            32: 64,
            33: 64,
            34: 65,
            35: 73,
            36: 83,
            37: 86,
            38: 87,
            39: 99,
            40: 108,
            41: 108,
            42: 101,
            43: 109,
            44: 122,
            45: 118,
            46: 131,
            47: 135,
            48: 140,
            49: 136,
            50: 153,
            51: 153,
            52: 166,
            53: 170,
            54: 175,
            55: 171,
            56: 192,
            57: 188,
            58: 197,
            59: 205,
            60: 218
        }[seq_len]


def prepare_data(seq_lengths: [int], bound_time: int) -> {int: [int]}:
    def equalize_data_lengths(data: [int], max_len: int) -> [int]:
        if len(data) > max_len:
            raise RuntimeError('Provided length should be greater than length of passed data...')
        else:
            return [data[idx] if idx < len(data) else data[-1] for idx in range(max_len)]

    sequences = {seq_len: run_sdls(seq_len, bound_time) for seq_len in seq_lengths}
    return {seq_len: equalize_data_lengths(seq_vals, len(max(sequences.values(), key=len))) for (seq_len, seq_vals) in
            sequences.items()}
    # return sequences


def prepare_plot(data):
    result = [go.Scatter(x=[idx * 5 for idx in range(0, len(seq_data))], y=seq_data, mode='lines',
                         name='Seq. length: %s' % seq_len) for seq_len, seq_data in data.items()]
    result.extend(
        [go.Scatter(x=[0, 5 * (len(seq_data) - 1)], y=[get_asymptote_value(seq_len) for _ in range(2)], mode='lines',
                    name='Seq. %s asymptote' % seq_len) for seq_len, seq_data in data.items()])

    return result


def upload_plot(data, bound_time, auto_open=False):
    print('Uploading data...')

    py.plot(go.Figure(data=data, layout=go.Layout(
        title='SDLS with bound time: %s [min]' % bound_time,
        xaxis=dict(
            title='Time [s]',
            titlefont=dict(
                family='Courier New, monospace',
                size=18,
                color='#7f7f7f'
            )
        ),
        yaxis=dict(
            title='Energy value',
            titlefont=dict(
                family='Courier New, monospace',
                size=18,
                color='#7f7f7f'
            )
        )
    )), file_name='sdls.%s' % time.time(), auto_open=auto_open)


def execute(seq_lengths, bound_time):
    # Proxy setup
    # os.environ['http_proxy'] = 'http://10.144.1.10:8080/'
    # os.environ['https_proxy'] = 'http://10.144.1.10:8080/'

    data = prepare_data(seq_lengths, bound_time)
    plot_data = prepare_plot(data)
    upload_plot(plot_data, bound_time)


if __name__ == '__main__':
    ARGS = docopt(__doc__, version='reporter 0.1')

    if ARGS['report']:
        seq_lengths = [int(seq_length) for seq_length in ARGS['<sequences>'].split(';')]
        bound_time = int(ARGS['<bound_time>'])
        print('------------------------')
        print('Calculating SDLS')
        print('Lengths: %s' % seq_lengths)
        print('Bound time: %s [min]' % bound_time)
        print('------------------------')

        execute(seq_lengths, bound_time)

    if ARGS['report_range']:
        raw_lengths = [int(seq_length) for seq_length in ARGS['<sequence_range>'].split(';')]
        sequence_step = int(ARGS['<sequence_step>'])
        bound_time = int(ARGS['<bound_time>'])

        seq_lengths = list(range(raw_lengths[0], raw_lengths[-1] + 1, sequence_step))
        print('------------------------')
        print('Calculating SDLS')
        print('Lengths: %s' % seq_lengths)
        print('Bound time: %s [min]' % bound_time)
        print('------------------------')

        execute(seq_lengths, bound_time)
