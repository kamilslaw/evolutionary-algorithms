from subprocess import call

time_sec = 600

for l in [50, 60]:
    for d in [1, 3, 5, 7, 20]:
        for i in range(5):
            output_file = 'result/o_{}_{}_{}.txt'.format(l, d, i)
            diagnostic_file = 'result/d_{}_{}_{}.txt'.format(l, d, i)
            call(['./target/release/sdls.exe', str(l), str(time_sec),
                str(d), output_file, diagnostic_file])