extern crate std;

use sequence::*;

pub struct SequenceWrapper {
    origin: u64,
    seq: Sequence,
    c: Vec<i32>,
    t: Vec<Vec<i32>>,
}

pub struct SdlsResult {
    pub power: i32,
    pub flips_count: i32,
}

impl SequenceWrapper {
    pub fn new(origin: u64, sequence_length: usize) -> SequenceWrapper {
        let seq = Sequence::from_integer(origin, sequence_length);
        let c = (1..seq.len()).map(|k| seq.get_autocorreletion(k)).collect();
        let t = (1..seq.len())
            .map(|k| {
                (0..seq.len() - k)
                    .map(|i| {
                        let v = seq.unwrap();
                        v[i] * v[i + k]
                    }).collect()
            }).collect();

        SequenceWrapper { origin, seq, c, t }
    }

    fn flip(&mut self, i: usize) {
        let v = self.seq.unwrap_mut();
        v[i] *= -1;
    }

    fn value_flip(&self, i: usize) -> i32 {
        let mut f = 0;
        let l = self.seq.len();

        for p in 0..l - 1 {
            let mut v = self.c[p];
            if p < l - 1 - i {
                v -= 2 * self.t[p][i];
            }

            if p < i {
                v -= 2 * self.t[p][i - p - 1];
            }

            f += v.pow(2);
        }

        f
    }

    pub fn sdls(&mut self) -> SdlsResult {
        let l = self.seq.len();
        let f_star = self.seq.power();
        let mut f_cross = std::i32::MAX;

        let mut flips_count = 0;

        for i in 0..l {
            let f_prim = self.value_flip(i);
            if f_prim < f_cross {
                f_cross = f_prim;

                flips_count += 1;
            }
        }

        SdlsResult {
            power: if f_star < f_cross { f_star } else { f_cross },
            flips_count,
        }
    }

    pub fn ext_sdls(&mut self, sdls_depth: u32) -> SdlsResult {
        let mut best_results_dec = Vec::with_capacity(sdls_depth as usize);
        best_results_dec.push(self.origin);

        let l = self.seq.len();
        let mut f_star = self.seq.power();

        let mut flips_count = 0;

        for _ in 0..sdls_depth {
            let mut f_cross = std::i32::MAX;
            let mut i_cross = 0;
            let mut dec_cross = 0;

            for i in 0..l {
                let f_prim = self.value_flip(i);
                if f_prim < f_cross {
                    let dec = self.seq_to_dec(i);
                    if !best_results_dec.iter().any(|&d| d == dec) {
                        f_cross = f_prim;
                        i_cross = i;
                        dec_cross = dec;

                        flips_count += 1;
                    }

                    if f_prim < f_star {
                        f_star = f_prim;
                    }
                }
            }

            best_results_dec.push(dec_cross);

            self.flip(i_cross);
            self.c = (1..self.seq.len()).map(|k| self.seq.get_autocorreletion(k)).collect();
            self.t = (1..self.seq.len())
                .map(|k| {
                    (0..self.seq.len() - k)
                        .map(|i| {
                            let v = self.seq.unwrap();
                            v[i] * v[i + k]
                        }).collect()
                }).collect();
        }

        SdlsResult {
            power: f_star,
            flips_count,
        }
    }

    fn seq_to_dec(&self, flip_index: usize) -> u64 {
        let mut dec = 0_u64;
        let vec = self.seq.unwrap();
        let l = vec.len();

        for i in 0..l {
            let digit = if i == flip_index { vec[i] * -1 } else { vec[i] };
            if digit == 1 {
                dec += 2_u64.pow((l - (i + 1)) as u32);
            }
        }

        dec
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_flip() {
        let sw = SequenceWrapper::new(22, 2);
        let seq = sw.flip(3);
        assert_eq!(vec![1, -1, 1, 1, -1], *sw.seq.unwrap());
        assert_eq!(vec![1, -1, 1, -1, -1], *seq.unwrap());
    }

    #[test]
    fn test_value_flip() {
        let sw = SequenceWrapper::new(22, 2);
        assert_eq!(6, sw.value_flip(3));
    }

    #[test]
    fn test_single_element_value_flip() {
        let sw = SequenceWrapper::new(1, 1);
        assert_eq!(0, sw.value_flip(0));
    }
}
