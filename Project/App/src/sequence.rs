use std::fmt;

pub struct Sequence {
    vec: Vec<i32>,
}

impl Sequence {
    #[allow(dead_code)]
    pub fn new(vec: Vec<i32>) -> Sequence {
        Sequence { vec }
    }

    pub fn from_integer(i: u64, sequence_length: usize) -> Sequence {
        let mut vec = Vec::with_capacity(sequence_length);
        let mut n = i;
        while n > 1 {
            vec.insert(0, if n % 2 == 0 { -1 } else { 1 });
            n /= 2;
        }

        vec.insert(0, 1);

        for _ in 0..sequence_length - vec.len() {
            vec.insert(0, -1);
        }

        Sequence { vec }
    }

    #[allow(dead_code)]
    pub fn parse(s: &str) -> Sequence {
        let vec = s.chars().map(|c| if c == '0' { -1 } else { 1 }).collect();
        Sequence { vec }
    }

    pub fn unwrap(&self) -> &Vec<i32> {
        &self.vec
    }

    pub fn unwrap_mut(&mut self) -> &mut Vec<i32> {
        &mut self.vec
    }

    pub fn len(&self) -> usize {
        self.vec.len()
    }

    pub fn get_autocorreletion(&self, k: usize) -> i32 {
        let l = self.vec.len();
        let mut sum = 0;
        if k < l {
            for i in 0..(l - k) {
                sum += self.vec[i] * self.vec[i + k];
            }
        }

        sum
    }

    pub fn power(&self) -> i32 {
        let l = self.vec.len();
        let mut sum = 0;
        for k in 1..l {
            sum += self.get_autocorreletion(k).pow(2);
        }

        sum
    }
}

impl fmt::Display for Sequence {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut fmt_str = "[".to_string();
        (0..self.len()).for_each(|i| {
            fmt_str.push_str(&format!("{}", if self.unwrap()[i] == 1 { 1 } else { 0 }))
        });
        fmt_str.push_str("]");
        write!(f, "{}", fmt_str)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_integer() {
        let seq = Sequence::from_integer(13, 6);
        assert_eq!(vec![-1, -1, 1, 1, -1, 1], *seq.unwrap());
    }

    #[test]
    fn test_autocorreletion() {
        let seq = Sequence::new(vec![1, 1, -1, 1, -1, 1]);
        let k = 2;
        assert_eq!(2, seq.get_autocorreletion(k));
    }

    #[test]
    fn test_single_element_autocorreletion() {
        let seq = Sequence::parse("1");
        let k = 2;
        assert_eq!(0, seq.get_autocorreletion(k));
    }

    #[test]
    fn test_power() {
        let seq = Sequence::parse("11101001");
        assert_eq!(8, seq.power());
    }

    #[test]
    fn test_single_element_power() {
        let seq = Sequence::parse("1");
        assert_eq!(0, seq.power());
    }
}
