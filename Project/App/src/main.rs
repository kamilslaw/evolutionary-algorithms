mod sequence;
mod sequence_wrapper;
mod time_executor;

use std::env;
use std::fs::File;
use std::io::{Error, ErrorKind};

fn main() -> std::io::Result<()> {
    let args: Vec<_> = env::args().collect();

    if args.len() == 2 {
        let sequence_length = args[1].parse::<usize>().unwrap();
        let count = 2_usize.pow(sequence_length as u32);
        for i in 0..count {
            let seq = sequence::Sequence::from_integer(i as u64, sequence_length);
            println!("{:2.0} : {:} : {:2.0}", i, seq, seq.power());
        }

        return Ok(());
    }

    if args.len() == 4 {
        let number = args[1].parse::<u64>().unwrap();
        let sequence_length = args[2].parse::<usize>().unwrap();
        let sdls_depth = args[3].parse::<u32>().unwrap();
        let mut seq = sequence_wrapper::SequenceWrapper::new(number, sequence_length);
        let result = if sdls_depth == 1 { seq.sdls() } else { seq.ext_sdls(sdls_depth) };
        println!("Power: {}", result.power);
        return Ok(());
    }

    if args.len() != 6 {
        eprintln!("  Usage:  > ./sdls sequence_length duration_in_seconds sdls_depth output_file diagnostic_output_file");
        return Err(Error::new(ErrorKind::Other, ""));
    }

    let sequence_length = args[1].parse::<usize>().unwrap();
    let duration_sec = args[2].parse::<u64>().unwrap();
    let sdls_depth = args[3].parse::<u32>().unwrap();
    let file_path = args[4].clone();
    let diagnostic_file_path = args[5].clone();

    let mut file = File::create(file_path)?;
    let mut diagnostic_file = File::create(diagnostic_file_path)?;

    time_executor::run(
        sequence_length,
        std::time::Duration::from_secs(duration_sec),
        sdls_depth,
        &mut file,
        &mut diagnostic_file,
    );

    file.sync_all()?;
    diagnostic_file.sync_all()?;

    Ok(())
}
