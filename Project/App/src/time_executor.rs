extern crate rand;

use sequence_wrapper::SequenceWrapper;
use std::i32;
use std::io::Write;
use std::time::{Duration, Instant};

pub fn run(sequence_length: usize, duration: Duration, sdls_depth: u32, output: &mut Write, diagnostic_output: &mut Write) -> i32 {
    let start = Instant::now();
    let range = 2_u64.pow(sequence_length as u32);
    let mut min_power = i32::MAX;
    
    let mut last_print: Option<Instant> = None;
    
    let mut iterations = 0;
    let mut total_flips_count = 0;

    while start.elapsed() < duration {
        let i = rand::random::<u64>() % range;
        let mut s = SequenceWrapper::new(i, sequence_length);
        let sdls = if sdls_depth == 1 { s.sdls() } else { s.ext_sdls(sdls_depth) };
        if sdls.power < min_power {
            min_power = sdls.power;
        }

        iterations += 1;
        total_flips_count += sdls.flips_count;

        if last_print.is_none() || last_print.unwrap().elapsed() > Duration::from_secs(5) {
            output.write_all(format!("{}\n", min_power).as_bytes()).unwrap();
            last_print = Some(Instant::now());
        }
    }

    output.write_all(format!("{}\n", min_power).as_bytes()).unwrap();

    diagnostic_output.write_all(format!("Seq length:    {}\n", sequence_length).as_bytes()).unwrap();
    diagnostic_output.write_all(format!("Duration [s]:  {}\n", duration.as_secs()).as_bytes()).unwrap();
    diagnostic_output.write_all(format!("SDLS depth:    {}\n", sdls_depth).as_bytes()).unwrap();
    diagnostic_output.write_all(b"=======================\n").unwrap();
    diagnostic_output.write_all(format!("Found power:   {}\n", min_power).as_bytes()).unwrap();
    diagnostic_output.write_all(format!("Min power:     {}\n", get_perfect_power(sequence_length)).as_bytes()).unwrap();
    diagnostic_output.write_all(b"=======================\n").unwrap();
    diagnostic_output.write_all(format!("Iterations:    {}\n", iterations).as_bytes()).unwrap();
    diagnostic_output.write_all(format!("Total flips:   {}\n", total_flips_count).as_bytes()).unwrap();
    diagnostic_output.write_all(format!("Avg flips:     {:.6}\n", total_flips_count as f64 / iterations as f64).as_bytes()).unwrap();

    min_power
}

fn get_perfect_power(sequence_length: usize) -> i32 {
    match sequence_length {
        4 => 2,
        5 => 2,
        6 => 7,
        7 => 3,
        8 => 8,
        9 => 12,
        10 => 13,
        11 => 5,
        12 => 10,
        13 => 6,
        14 => 19,
        15 => 15,
        16 => 24,
        17 => 32,
        18 => 25,
        19 => 29,
        20 => 26,
        21 => 26,
        22 => 39,
        23 => 47,
        24 => 36,
        25 => 36,
        26 => 45,
        27 => 37,
        28 => 50,
        29 => 62,
        30 => 59,
        31 => 67,
        32 => 64,
        33 => 64,
        34 => 65,
        35 => 73,
        36 => 82,
        37 => 86,
        38 => 87,
        39 => 99,
        40 => 108,
        41 => 108,
        42 => 101,
        43 => 109,
        44 => 122,
        45 => 118,
        46 => 131,
        47 => 135,
        48 => 140,
        49 => 136,
        50 => 153,
        51 => 153,
        52 => 166,
        53 => 170,
        54 => 175,
        55 => 171,
        56 => 192,
        57 => 188,
        58 => 197,
        59 => 205,
        60 => 218,
        _ => 0,
    }
}
