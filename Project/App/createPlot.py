import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import sys
import datetime
import statistics

plotly.tools.set_credentials_file(
    username='kamilslaw', api_key='fdTbL1sfrNlmxCm47kXV')

l = 60
data = []

for d in [1, 3, 5, 7, 20]:
    ints = []
    for i in range(5):
        output_file = 'result/o_{}_{}_{}.txt'.format(l, d, i)
        with open(output_file) as f:
            ints.append(list(map(int, f)))
    means = [int(sum(l)) / len(l) for l in zip(*ints)]
    data.append(go.Scatter(
        x= list(range(0, 5 * len(means), 5)),
        y=means,
        name=str(d)
    ))

layout = go.Layout(
    xaxis=dict(title='Time [s]'),
    yaxis=dict(title='Min power')
)
fig = go.Figure(data=data, layout=layout)
py.plot(fig, filename='plot ' + str(datetime.datetime.now()))
