#!/bin/bash

cd ~/Pobrane/jclec4-base/

for i in {1..5}
do

 /usr/lib/jvm/java-11-openjdk-amd64/bin/java -Dfile.encoding=UTF-8 \
 -classpath /home/kamil/Pobrane/jclec4-base/target/classes:/home/kamil/Pobrane/jclec4-base/libs/commons-collections-3.2.1.jar:/home/kamil/Pobrane/jclec4-base/libs/commons-configuration-1.5.jar:/home/kamil/Pobrane/jclec4-base/libs/commons-lang-2.4.jar:/home/kamil/Pobrane/jclec4-base/libs/commons-logging-1.1.1.jar:/home/kamil/Pobrane/jclec4-base/libs/junit-3.8.2.jar net.sf.jclec.RunExperiment \
 ~/Pobrane/jclec4-base/Rastrigin.cfg \
| grep Best | grep -Po 'value=\d+\.\d+' | sed -e 's/value=//g' > ~/evolutionary-algorithms/Labs/3/data/nonUniformMutator_08$i.txt
sleep 1

done
