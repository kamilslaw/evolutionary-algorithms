import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import sys
import datetime
import statistics

plotly.tools.set_credentials_file(
    username='kamilslaw', api_key='fdTbL1sfrNlmxCm47kXV')

selectors = ["nonUniformMutator_001", "nonUniformMutator_01", "nonUniformMutator_02", "nonUniformMutator_05", "nonUniformMutator_08"]
data = []

for s in selectors:
    floats = []
    for i in range(1, 5):
        with open("data/" + s + str(i) + ".txt") as f:
            floats.append(map(float, f))
    means = [float(sum(l))/len(l) for l in zip(*floats)]
    std = [statistics.stdev(l) for l in zip(*floats)]
    data.append(go.Scatter(
        y=means,
        name=s,
        error_y=dict(
            type='data',
            array=std,
            visible=True
        )
    ))

py.plot(data, filename='plot ' + str(datetime.datetime.now()))
